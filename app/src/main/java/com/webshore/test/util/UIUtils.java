package com.webshore.test.util;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.ColorMatrixColorFilter;
import android.os.Build;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.webshore.test.R;

import java.util.regex.Pattern;

import static com.webshore.test.util.LogUtils.LOGE;
import static com.webshore.test.util.LogUtils.makeLogTag;

public class UIUtils {
    private static final String TAG = makeLogTag(UIUtils.class);

    public static final float CAR_BG_COLOR_SCALE_FACTOR = 0.75f;

    private static final float CAR_PHOTO_SCRIM_ALPHA = 0.5f;
    private static final float CAR_PHOTO_SCRIM_SATURATION = 0.2f;

    private static final Pattern REGEX_HTML_ESCAPE = Pattern.compile(".*&\\S;.*");

    public static int setColorAlpha(int color, float alpha) {
        int alpha_int = Math.min(Math.max((int) (alpha * 255.0f), 0), 255);

        return Color.argb(alpha_int, Color.red(color), Color.green(color), Color.blue(color));
    }

    public static int scaleColor(int color, float factor, boolean scaleAlpha) {
        return Color.argb(scaleAlpha ? (Math.round(Color.alpha(color) * factor)) : Color.alpha(color),
                Math.round(Color.red(color) * factor), Math.round(Color.green(color) * factor),
                Math.round(Color.blue(color) * factor));
    }

    public static int scaleCarColorToDefaultBG(int color) {
        return scaleColor(color, CAR_BG_COLOR_SCALE_FACTOR, false);
    }

    public static void setAccessibilityIgnore(View view) {
        view.setClickable(false);

        view.setFocusable(false);

        view.setContentDescription("");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            view.setImportantForAccessibility(View.IMPORTANT_FOR_ACCESSIBILITY_NO);
        }
    }

    private static final int[] RES_IDS_ACTION_BAR_SIZE = {R.attr.actionBarSize};

    public static void setTextMaybeHtml(TextView view, String text) {
        if (TextUtils.isEmpty(text)) {
            view.setText("");

            return;
        }

        if ((text.contains("<") && text.contains(">")) || REGEX_HTML_ESCAPE.matcher(text).find()) {
            view.setText(Html.fromHtml(text));

            view.setMovementMethod(LinkMovementMethod.getInstance());
        } else {
            view.setText(text);
        }
    }

    public static int calculateActionBarSize(Context context) {
        if (context == null) {
            return 0;
        }

        Resources.Theme curTheme = context.getTheme();

        if (curTheme == null) {
            return 0;
        }

        TypedArray att = curTheme.obtainStyledAttributes(RES_IDS_ACTION_BAR_SIZE);

        if (att == null) {
            return 0;
        }

        float size = att.getDimension(0, 0);

        att.recycle();

        return (int) size;
    }

    public static void setUpButterBar(View butterBar, String messageText, String actionText,
                                      View.OnClickListener listener) {
        if (butterBar == null) {
            LOGE(TAG, "Failed to set up butter bar: it's null.");

            return;
        }

        TextView textView = (TextView) butterBar.findViewById(R.id.butter_bar_text);

        if (textView != null) {
            textView.setText(messageText);
        }

        Button button = (Button) butterBar.findViewById(R.id.butter_bar_button);

        if (button != null) {
            button.setText(actionText == null ? "" : actionText);

            button.setVisibility(!TextUtils.isEmpty(actionText) ? View.VISIBLE : View.GONE);
        }

        button.setOnClickListener(listener);

        butterBar.setVisibility(View.VISIBLE);
    }

    public static float getProgress(int value, int min, int max) {
        if (min == max) {
            throw new IllegalArgumentException("Max (" + max + ") cannot equal min (" + min + ")");
        }

        return (value - min) / (float) (max - min);
    }

    public static ColorFilter makeCarImageScrimColorFilter(int carColor) {
        float a = CAR_PHOTO_SCRIM_ALPHA;

        float sat = CAR_PHOTO_SCRIM_SATURATION;

        return new ColorMatrixColorFilter(new float[]{
                ((1 - 0.213f) * sat + 0.213f) * a, ((0 - 0.715f) * sat + 0.715f) * a, ((0 - 0.072f) * sat + 0.072f) * a, 0, Color.red(carColor) * (1 - a),
                ((0 - 0.213f) * sat + 0.213f) * a, ((1 - 0.715f) * sat + 0.715f) * a, ((0 - 0.072f) * sat + 0.072f) * a, 0, Color.green(carColor) * (1 - a),
                ((0 - 0.213f) * sat + 0.213f) * a, ((0 - 0.715f) * sat + 0.715f) * a, ((1 - 0.072f) * sat + 0.072f) * a, 0, Color.blue(carColor) * (1 - a),
                0, 0, 0, 0, 255
        });
    }
}
