package com.webshore.test.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.webshore.test.R;

public class HelpUtils {
    private static final String WEBSHORE_URL = "http://www.webshore-maroc.com/";

    public static void showAbout(Activity activity) {
        FragmentManager fm = activity.getFragmentManager();

        FragmentTransaction ft = fm.beginTransaction();

        Fragment prev = fm.findFragmentByTag("dialog_about");

        if (prev != null) {
            ft.remove(prev);
        }

        ft.addToBackStack(null);

        new AboutDialog().show(ft, "dialog_about");
    }

    public static class AboutDialog extends DialogFragment {
        private static final String VERSION_UNAVAILABLE = "N/A";

        public AboutDialog() {
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            PackageManager pm = getActivity().getPackageManager();

            String packageName = getActivity().getPackageName();

            String versionName;

            try {
                PackageInfo info = pm.getPackageInfo(packageName, 0);

                versionName = info.versionName;
            } catch (PackageManager.NameNotFoundException e) {
                versionName = VERSION_UNAVAILABLE;
            }

            SpannableStringBuilder aboutBody = new SpannableStringBuilder();

            aboutBody.append(Html.fromHtml(getString(R.string.about_body, versionName)));

            SpannableString developedFor = new SpannableString(getString(R.string.developed_for));

            developedFor.setSpan(new ClickableSpan() {
                @Override
                public void onClick(View view) {
                    Intent i = new Intent(Intent.ACTION_VIEW);

                    i.setData(Uri.parse(WEBSHORE_URL));

                    startActivity(i);
                }
            }, 42, developedFor.length() - 1, 0);

            aboutBody.append(developedFor);

            SpannableString developedFor2 = new SpannableString(getString(R.string.developed_for_2));

            aboutBody.append(developedFor2);

            SpannableString developedFor3 = new SpannableString(getString(R.string.developed_for_3));

            aboutBody.append(developedFor3);

            LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);

            View aboutBodyLayout = layoutInflater.inflate(R.layout.dialog_about, null);

            TextView aboutBodyView = (TextView) aboutBodyLayout.findViewById(android.R.id.text1);

            aboutBodyView.setText(aboutBody);

            aboutBodyView.setMovementMethod(new LinkMovementMethod());

            AlertDialog.Builder builder;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                builder = new AlertDialog.Builder(getActivity());
            } else {
                builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.AlertDialogCustom));
            }

            return builder.setTitle(R.string.title_about)
                    .setView(aboutBodyLayout)
                    .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create();
        }
    }
}
