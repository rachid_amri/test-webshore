package com.webshore.test.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;

import com.webshore.test.R;

public class RecentTasksStyler {
    private static Bitmap sIcon = null;

    private RecentTasksStyler() {
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static void styleRecentTasksEntry(Activity activity) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            return;
        }

        Resources resources = activity.getResources();

        String label = resources.getString(activity.getApplicationInfo().labelRes);

        int colorPrimary = resources.getColor(R.color.theme_primary);

        if (sIcon == null) {
            // TODO: Change the status notification icon
            sIcon = BitmapFactory.decodeResource(resources, R.drawable.ic_stat_notification);
        }

        activity.setTaskDescription(new ActivityManager.TaskDescription(label, sIcon, colorPrimary));
    }
}
