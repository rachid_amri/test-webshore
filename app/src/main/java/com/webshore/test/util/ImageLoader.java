package com.webshore.test.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import com.webshore.test.R;
import com.bumptech.glide.BitmapRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.ModelRequest;
import com.bumptech.glide.load.Transformation;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.ModelCache;
import com.bumptech.glide.load.model.stream.BaseGlideUrlLoader;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.request.bitmap.RequestListener;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.webshore.test.util.LogUtils.LOGD;
import static com.webshore.test.util.LogUtils.makeLogTag;

public class ImageLoader {
    private static final String TAG = makeLogTag(ImageLoader.class);

    private static final ModelCache<GlideUrl> urlCache = new ModelCache<GlideUrl>(150);

    private final ModelRequest.ImageModelRequest<String> mGlideModelRequest;

    private final CenterCrop mCenterCrop;

    private final Transformation<Bitmap> mNone;

    private int mPlaceHolderResId = -1;

    public ImageLoader(Context context) {
        VariableWidthImageLoader imageLoader = new VariableWidthImageLoader(context);

        mGlideModelRequest = Glide.with(context).using(imageLoader);

        mCenterCrop = new CenterCrop(Glide.get(context).getBitmapPool());

        mNone = Transformation.NONE;
    }

    public ImageLoader(Context context, int placeHolderResId) {
        this(context);

        mPlaceHolderResId = placeHolderResId;
    }

    public void loadImage(String url, ImageView imageView, RequestListener<String> requestListener) {
        loadImage(url, imageView, requestListener, null, false);
    }

    public void loadImage(String url, ImageView imageView, RequestListener<String> requestListener,
                          Drawable placholderOverride) {
        loadImage(url, imageView, requestListener, placholderOverride, false);
    }

    public void loadImage(String url, ImageView imageView, RequestListener<String> requestListener,
                          Drawable placeholderOverride, boolean crop) {
        BitmapRequestBuilder request = beginImageLoad(url, requestListener, crop)
                .animate(R.anim.image_fade_in);

        if (placeholderOverride != null) {
            request.placeholder(placeholderOverride);
        } else if (mPlaceHolderResId != -1) {
            request.placeholder(mPlaceHolderResId);
        }

        request.into(imageView);
    }

    public BitmapRequestBuilder beginImageLoad(String url,
                                               RequestListener<String> requestListener, boolean crop) {
        return mGlideModelRequest.load(url)
                .asBitmap()
                .listener(requestListener)
                .transform(crop ? mCenterCrop : mNone);
    }

    public void loadImage(String url, ImageView imageView) {
        loadImage(url, imageView, false);
    }

    public void loadImage(String url, ImageView imageView, boolean crop) {
        loadImage(url, imageView, null, null, crop);
    }

    private static class VariableWidthImageLoader extends BaseGlideUrlLoader<String> {
        private static final Pattern PATTERN = Pattern.compile("__w-((?:-?\\d+)+)__");

        public VariableWidthImageLoader(Context context) {
            super(context, urlCache);
        }

        @Override
        public String getId(String model) {
            return model;
        }

        @Override
        protected String getUrl(String model, int width, int height) {
            Matcher m = PATTERN.matcher(model);

            int bestBucket = 0;

            if (m.find()) {
                String[] found = m.group(1).split("-");

                for (String bucketStr : found) {
                    bestBucket = Integer.parseInt(bucketStr);

                    if (bestBucket >= width) {
                        break;
                    }
                }

                if (bestBucket > 0) {
                    model = m.replaceFirst("w" + bestBucket);

                    LOGD(TAG, "width=" + width + ", URL successfully replaced by " + model);
                }
            }

            return model;
        }
    }
}
