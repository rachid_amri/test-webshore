package com.webshore.test.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import static com.webshore.test.util.LogUtils.makeLogTag;

public class PrefUtils {
    public static final String PREF_WELCOME_DONE = "pref_welcome_done";

    public static final String PREF_WELCOME_INITIATED = "pref_welcome_initiated";

    public static final String PREF_FIRST_INTERNET_WARNING = "pref_first_internet_warning";

    public static final String PREF_SECOND_INTERNET_WARNING = "pref_second_internet_warning";

    private static final String TAG = makeLogTag("PrefUtils");

    public static boolean isWelcomeDone(final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        return sp.getBoolean(PREF_WELCOME_DONE, false);
    }

    public static void markWelcomeDone(final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        sp.edit().putBoolean(PREF_WELCOME_DONE, true).commit();
    }

    public static boolean isWelcomeInitiated(final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        return sp.getBoolean(PREF_WELCOME_INITIATED, false);
    }

    public static void markWelcomeInitiated(final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        sp.edit().putBoolean(PREF_WELCOME_INITIATED, true).commit();
    }

    public static boolean isCarFavorite(final Context context, String carId) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        return sp.getBoolean(carId, false);
    }

    public static void markCarFavorite(final Context context, String carId, boolean isFavorite) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        sp.edit().putBoolean(carId, isFavorite).commit();
    }

    public static boolean hasGivenFeedback(final Context context, String carId) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        return sp.getBoolean(carId + "feedback", false);
    }

    public static void setGaveFeedback(final Context context, String carId, boolean gaveFeedback) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        sp.edit().putBoolean(carId + "feedback", gaveFeedback).commit();
    }

    public static boolean hasShownFirstInternetWarning(final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        return sp.getBoolean(PREF_FIRST_INTERNET_WARNING, false);
    }

    public static void setFirstInternetWarningShown(final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        sp.edit().putBoolean(PREF_FIRST_INTERNET_WARNING, true).commit();
    }

    public static boolean hasShownSecondInternetWarning(final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        return sp.getBoolean(PREF_SECOND_INTERNET_WARNING, false);
    }

    public static void setSecondInternetWarningShown(final Context context) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        sp.edit().putBoolean(PREF_SECOND_INTERNET_WARNING, true).commit();
    }

    public static void registerOnSharedPreferenceChangeListener(final Context context,
                                                                SharedPreferences.OnSharedPreferenceChangeListener listener) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        sp.registerOnSharedPreferenceChangeListener(listener);
    }

    public static void unregisterOnSharedPreferenceChangeListener(final Context context,
                                                                  SharedPreferences.OnSharedPreferenceChangeListener listener) {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);

        sp.unregisterOnSharedPreferenceChangeListener(listener);
    }
}
