package com.webshore.test;

public class Config {
    public static final boolean IS_DOGFOOD_BUILD = true;

    public interface Tags {
        String CATEGORY_MANUFACTURERS = "CATEGORY_MANUFACTURERS";

        String[] BROWSE_CATEGORIES = {CATEGORY_MANUFACTURERS};

        int[] BROWSE_CATEGORY_TITLE = {R.string.manufacturers};
    }
}
