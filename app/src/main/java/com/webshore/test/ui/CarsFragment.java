package com.webshore.test.ui;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.support.v4.view.ViewCompat;
import android.text.Html;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.webshore.test.R;
import com.webshore.test.model.Car;
import com.webshore.test.model.TagMetadata;
import com.webshore.test.provider.TestContract;
import com.webshore.test.ui.widget.CollectionView;
import com.webshore.test.ui.widget.CollectionViewCallbacks;
import com.webshore.test.util.ImageLoader;
import com.webshore.test.util.PrefUtils;
import com.webshore.test.util.UIUtils;
import com.bumptech.glide.GenericRequestBuilder;
import com.bumptech.glide.ListPreloader;
import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import static com.webshore.test.util.LogUtils.LOGD;
import static com.webshore.test.util.LogUtils.LOGV;
import static com.webshore.test.util.LogUtils.makeLogTag;

public class CarsFragment extends Fragment implements CollectionViewCallbacks {
    private static final String TAG = makeLogTag(CarsFragment.class);

    public static final String EXTRA_NO_TRACK_BRANDING =
            "com.webshore.test.extra.NO_TRACK_BRANDING";

    public static final String EXTRA_TAG =
            "com.webshore.test.extra.TAG";

    private static final String STATE_CARS_QUERY_TOKEN = "cars_query_token";

    private static final String STATE_ARGUMENTS = "arguments";

    private static final String STATE_HERO_INDEX = "hero_index";

    private static final int ROWS_TO_PRELOAD = 2;

    private static final int ANIM_DURATION = 250;

    private int mSessionQueryToken;

    private Uri mCurrentUri = TestContract.Cars.CONTENT_URI;

    private String mCurrentTag;

    private ArrayList<Car> mCars;

    private ArrayList<Car> mFilteredCars;

    private boolean mNoTrackBranding;

    private ImageLoader mImageLoader;

    private int mDefaultCarColor;

    private CollectionView mCollectionView;

    private LinearLayout mEmptyView;

    private TagMetadata mTagMetadata = null;

    private boolean mWasPaused = false;

    private static final int HERO_GROUP_ID = 123;

    private Bundle mArguments;

    private Preloader mPreloader;

    public interface Callbacks {
        void onCarSelected(String carId, View clickedView);

        void onTagMetadataLoaded(TagMetadata metadata);
    }

    private static Callbacks sDummyCallbacks = new Callbacks() {
        @Override
        public void onCarSelected(String carId, View clickedView) {
        }

        @Override
        public void onTagMetadataLoaded(TagMetadata metadata) {
        }
    };

    private Callbacks mCallbacks = sDummyCallbacks;

    private Handler mHandler;

    private int mHeroIndex;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Dao<Car, UUID> carsDao = null;

        try {
            if (getActivity() instanceof BrowseCarsActivity) {
                carsDao = ((BrowseCarsActivity) getActivity()).getDatabaseHelper().getCarsDao();
            } else {
                carsDao = ((SearchActivity) getActivity()).getDatabaseHelper().getCarsDao();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (mImageLoader == null) {
            mImageLoader = new ImageLoader(this.getActivity());
        }

        mDefaultCarColor = getResources().getColor(R.color.default_car_color);

        if (savedInstanceState != null) {
            mSessionQueryToken = savedInstanceState.getInt(STATE_CARS_QUERY_TOKEN);

            mArguments = savedInstanceState.getParcelable(STATE_ARGUMENTS);

            if (mArguments != null) {
                mCurrentUri = mArguments.getParcelable("_uri");

                mNoTrackBranding = mArguments.getBoolean(EXTRA_NO_TRACK_BRANDING);
            }
        }

        mTagMetadata = new TagMetadata(getActivity());

        List<Car> carList = null;

        try {
            carList = carsDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        mCars = new ArrayList<>();

        for (Car car : carList) {
            if (Integer.parseInt(car.getId()) < 100) {
                car.setColor(getResources().getColor(R.color.peugeot_tag_color));
            } else if (Integer.parseInt(car.getId()) < 200) {
                car.setColor(getResources().getColor(R.color.renault_tag_color));
            } else if (Integer.parseInt(car.getId()) < 300) {
                car.setColor(getResources().getColor(R.color.seat_tag_color));
            } else if (Integer.parseInt(car.getId()) < 400) {
                car.setColor(getResources().getColor(R.color.mazda_tag_color));
            } else if (Integer.parseInt(car.getId()) < 500) {
                car.setColor(getResources().getColor(R.color.bmw_tag_color));
            } else if (Integer.parseInt(car.getId()) < 600) {
                car.setColor(getResources().getColor(R.color.chevrolet_tag_color));
            } else if (Integer.parseInt(car.getId()) < 700) {
                car.setColor(getResources().getColor(R.color.mercedes_tag_color));
            } else if (Integer.parseInt(car.getId()) < 800) {
                car.setColor(getResources().getColor(R.color.audi_tag_color));
            } else if (Integer.parseInt(car.getId()) < 900) {
                car.setColor(getResources().getColor(R.color.lexus_tag_color));
            }

            mCars.add(car);
        }

        if (savedInstanceState != null) {
            mHeroIndex = savedInstanceState.getInt(STATE_HERO_INDEX);
        } else {
            mHeroIndex = (int) (Math.random() * mCars.size());
        }

        Car tempCar = mCars.get(mHeroIndex);

        mCars.set(0, tempCar);

        mCars.remove(mHeroIndex);

        mHandler = new Handler();

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mCallbacks.onTagMetadataLoaded(mTagMetadata);

                updateCollectionView(true);
            }
        }, 0);
    }

    private boolean useExpandedMode() {
        if (mCurrentUri != null && TestContract.Cars.CONTENT_URI.equals(mCurrentUri)) {
            return false;
        }

        return true;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_cars, container, false);

        mCollectionView = (CollectionView) root.findViewById(R.id.cars_collection_view);

        mPreloader = new Preloader(ROWS_TO_PRELOAD);

        mCollectionView.setOnScrollListener(mPreloader);

        mEmptyView = (LinearLayout) root.findViewById(R.id.empty);

        return root;
    }

    void reloadFromArguments(Bundle arguments, boolean fullReload) {
        if (arguments == null) {
            arguments = new Bundle();
        } else {
            arguments = (Bundle) arguments.clone();
        }

        mArguments = arguments;

        LOGD(TAG, "CarsFragment reloading from arguments: " + arguments);

        mCurrentUri = arguments.getParcelable("_uri");

        mCurrentTag = arguments.getString(EXTRA_TAG);

        mFilteredCars = new ArrayList<>(mCars);

        Iterator<Car> carIterator = mFilteredCars.iterator();

        if (mCurrentTag != null) {
            switch (mCurrentTag) {
                case "FAVORITES":
                    while (carIterator.hasNext()) {
                        if (!PrefUtils.isCarFavorite(getActivity(), carIterator.next().getId())) {
                            carIterator.remove();
                        }
                    }

                    break;
                case "0":
                    while (carIterator.hasNext()) {
                        if (carIterator.next().getColor() != getResources().getColor(R.color.peugeot_tag_color)) {
                            carIterator.remove();
                        }
                    }

                    break;
                case "1":
                    while (carIterator.hasNext()) {
                        if (carIterator.next().getColor() != getResources().getColor(R.color.renault_tag_color)) {
                            carIterator.remove();
                        }
                    }

                    break;
                case "2":
                    while (carIterator.hasNext()) {
                        if (carIterator.next().getColor() != getResources().getColor(R.color.seat_tag_color)) {
                            carIterator.remove();
                        }
                    }

                    break;
                case "3":
                    while (carIterator.hasNext()) {
                        if (carIterator.next().getColor() != getResources().getColor(R.color.mazda_tag_color)) {
                            carIterator.remove();
                        }
                    }

                    break;
                case "4":
                    while (carIterator.hasNext()) {
                        if (carIterator.next().getColor() != getResources().getColor(R.color.bmw_tag_color)) {
                            carIterator.remove();
                        }
                    }

                    break;
                case "5":
                    while (carIterator.hasNext()) {
                        if (carIterator.next().getColor() != getResources().getColor(R.color.chevrolet_tag_color)) {
                            carIterator.remove();
                        }
                    }

                    break;
                case "6":
                    while (carIterator.hasNext()) {
                        if (carIterator.next().getColor() != getResources().getColor(R.color.mercedes_tag_color)) {
                            carIterator.remove();
                        }
                    }

                    break;
                case "7":
                    while (carIterator.hasNext()) {
                        if (carIterator.next().getColor() != getResources().getColor(R.color.audi_tag_color)) {
                            carIterator.remove();
                        }
                    }

                    break;
                case "8":
                    while (carIterator.hasNext()) {
                        if (carIterator.next().getColor() != getResources().getColor(R.color.lexus_tag_color)) {
                            carIterator.remove();
                        }
                    }

                    break;
            }
        } else if (!TextUtils.isEmpty(mArguments.getString("QUERY"))) {
            String queryString = mArguments.getString("QUERY").toLowerCase();

            while (carIterator.hasNext()) {
                Car car = carIterator.next();

                if (!car.getTitle().toLowerCase().contains(queryString) &&
                        !car.getDescription().toLowerCase().contains(queryString)) {
                    carIterator.remove();
                }
            }
        }

        if (mCurrentUri == null) {
            LOGD(TAG, "CarsFragment did not get a URL, defaulting to all cars.");

            arguments.putParcelable("_uri", TestContract.Cars.CONTENT_URI);

            mCurrentUri = TestContract.Cars.CONTENT_URI;
        }

        mNoTrackBranding = mArguments.getBoolean(EXTRA_NO_TRACK_BRANDING);

        LOGD(TAG, "CarsFragment reloading, uri=" + mCurrentUri + ", expanded=" + useExpandedMode());

        updateCollectionView(fullReload);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (!(activity instanceof Callbacks)) {
            throw new ClassCastException("Activity must implement fragment's callbacks.");
        }

        mCallbacks = (Callbacks) activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();

        mCallbacks = sDummyCallbacks;
    }

    @Override
    public void onPause() {
        super.onPause();

        mWasPaused = true;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mWasPaused) {
            mWasPaused = false;

            LOGD(TAG, "Reloading data as a result of onResume()");

            if (mCurrentTag != null && mCurrentTag.equals("FAVORITES")) {
                reloadFromArguments(mArguments, false);
            } else {
                updateCollectionView(false);
            }
        }
    }

    public void animateReload() {
        mCollectionView.setAlpha(0);

        mCollectionView.animate().alpha(1).setDuration(ANIM_DURATION).setInterpolator(new DecelerateInterpolator());
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putInt(STATE_CARS_QUERY_TOKEN, mSessionQueryToken);

        outState.putParcelable(STATE_ARGUMENTS, mArguments);

        outState.putInt(STATE_HERO_INDEX, mHeroIndex);
    }

    public boolean canCollectionViewScrollUp() {
        return ViewCompat.canScrollVertically(mCollectionView, -1);
    }

    public void updateCollectionView(boolean fullReload) {
        LOGD(TAG, "CarsFragment updating CollectionView... " + (fullReload ?
                "(FULL RELOAD)" : "(light refresh)"));

        mMaxDataIndexAnimated = 0;

        CollectionView.Inventory inv;

        inv = prepareInventory();

        if (mFilteredCars.size() == 0) {
            mEmptyView.setVisibility(View.VISIBLE);
        } else {
            mEmptyView.setVisibility(View.GONE);
        }

        Parcelable state = null;

        if (!fullReload) {
            state = mCollectionView.onSaveInstanceState();
        }

        LOGD(TAG, "Updating CollectionView with inventory, # groups = " + inv.getGroupCount()
                + " total items = " + inv.getTotalItemCount());

        mCollectionView.setCollectionAdapter(this);

        mCollectionView.updateInventory(inv, fullReload);

        if (state != null) {
            mCollectionView.onRestoreInstanceState(state);
        }
    }

    private CollectionView.Inventory prepareInventory() {
        LOGD(TAG, "Preparing collection view inventory.");

        ArrayList<CollectionView.InventoryGroup> groups =
                new ArrayList<CollectionView.InventoryGroup>();

        HashMap<String, CollectionView.InventoryGroup> groupsByName =
                new HashMap<String, CollectionView.InventoryGroup>();

        CollectionView.InventoryGroup heroGroup = null;

        int nextGroupId = HERO_GROUP_ID + 1000;

        final boolean expandedMode = useExpandedMode();

        final int displayCols = getResources().getInteger(expandedMode ?
                R.integer.browse_2nd_level_grid_columns : R.integer.browse_1st_level_grid_columns);

        LOGD(TAG, "Using " + displayCols + " columns.");

        mPreloader.setDisplayCols(displayCols);

        if (mFilteredCars == null) {
            reloadFromArguments(mArguments, true);
        }

        for (int dataIndex = 0; dataIndex < mFilteredCars.size(); dataIndex++) {
            String groupLabel = "";

            CollectionView.InventoryGroup group;

            if (!useExpandedMode() && heroGroup == null) {
                LOGV(TAG, "This item is the hero.");

                group = heroGroup = new CollectionView.InventoryGroup(HERO_GROUP_ID)
                        .setDisplayCols(1)
                        .setShowHeader(false).setHeaderLabel("");
            } else {
                if (!groupsByName.containsKey(groupLabel)) {
                    LOGV(TAG, "Creating new group: " + groupLabel);

                    group = new CollectionView.InventoryGroup(nextGroupId++)
                            .setDisplayCols(displayCols)
                            .setShowHeader(!TextUtils.isEmpty(groupLabel))
                            .setHeaderLabel(groupLabel);

                    groupsByName.put(groupLabel, group);

                    groups.add(group);
                } else {
                    LOGV(TAG, "Adding to existing group: " + groupLabel);

                    group = groupsByName.get(groupLabel);
                }
            }

            LOGV(TAG, "...adding to group '" + groupLabel + "' with custom data index " + dataIndex);

            group.addItemWithCustomDataIndex(dataIndex);
        }

        ArrayList<CollectionView.InventoryGroup> allGroups = new ArrayList<CollectionView.InventoryGroup>();

        if (heroGroup != null) {
            allGroups.add(heroGroup);
        }

        allGroups.addAll(groups);

        LOGD(TAG, "Total: hero " + (heroGroup == null ? "absent" : "present")
                + " " + groups.size() + "groups, total " + allGroups.size());

        CollectionView.Inventory inventory = new CollectionView.Inventory();

        for (CollectionView.InventoryGroup g : allGroups) {
            inventory.addGroup(g);
        }

        return inventory;
    }

    @Override
    public View newCollectionHeaderView(Context context, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);

        return inflater.inflate(R.layout.list_item_browse_header, parent, false);
    }

    @Override
    public void bindCollectionHeaderView(Context context, View view, int groupId, String groupLabel) {
        TextView tv = (TextView) view.findViewById(android.R.id.text1);

        if (tv != null) {
            tv.setText(groupLabel);
        }
    }

    @Override
    public View newCollectionItemView(Context context, int groupId, ViewGroup parent) {
        final LayoutInflater inflater = (LayoutInflater) context.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE);

        int layoutId;

        if (useExpandedMode()) {
            layoutId = R.layout.list_item_car;
        } else {
            layoutId = (groupId == HERO_GROUP_ID) ? R.layout.list_item_car_hero : R.layout.list_item_car_summarized;
        }

        return inflater.inflate(layoutId, parent, false);
    }

    private int mMaxDataIndexAnimated = 0;

    @Override
    public void bindCollectionItemView(Context context, View view, int groupId, int indexInGroup, int dataIndex, Object tag) {
        Car car = mFilteredCars.get(dataIndex);

        final String carId = car.getId();

        if (carId == null) {
            return;
        }

        final String carTitle = car.getTitle();

        final String carDescription = car.getDescription();

        int color = car.getColor();

        color = color == 0 ? getResources().getColor(R.color.default_car_color)
                : color;

        int darkCarColor = 0;

        final boolean checked = PrefUtils.isCarFavorite(getActivity(), carId);

        final TextView titleView = (TextView) view.findViewById(R.id.car_title);
        final TextView subtitleView = (TextView) view.findViewById(R.id.car_subtitle);
        final TextView shortSubtitleView = (TextView) view.findViewById(R.id.car_subtitle_short);
        final TextView snippetView = (TextView) view.findViewById(R.id.car_snippet);
        final TextView abstractView = (TextView) view.findViewById(R.id.car_description);
        final TextView categoryView = (TextView) view.findViewById(R.id.car_category);
        final View carTargetView = view.findViewById(R.id.car_target);

        if (color == 0) {
            color = mDefaultCarColor;
        }

        darkCarColor = UIUtils.scaleCarColorToDefaultBG(color);

        ImageView photoView = (ImageView) view.findViewById(R.id.car_photo_colored);

        if (photoView != null) {
            if (!mPreloader.isDimensSet()) {
                final ImageView finalPhotoView = photoView;

                photoView.post(new Runnable() {
                    @Override
                    public void run() {
                        mPreloader.setDimens(finalPhotoView.getWidth(), finalPhotoView.getHeight());
                    }
                });
            }

            photoView.setColorFilter(mNoTrackBranding
                    ? new PorterDuffColorFilter(
                    getResources().getColor(R.color.no_track_branding_car_tile_overlay),
                    PorterDuff.Mode.SRC_ATOP)
                    : UIUtils.makeCarImageScrimColorFilter(darkCarColor));
        } else {
            photoView = (ImageView) view.findViewById(R.id.car_photo);
        }

        ViewCompat.setTransitionName(photoView, "photo_" + carId);

        ViewParent parent = photoView.getParent();

        if (parent != null && parent instanceof View) {
            ((View) parent).setBackgroundColor(darkCarColor);
        } else {
            photoView.setBackgroundColor(darkCarColor);
        }

        String photo = car.getPhotoUrl();

        if (!TextUtils.isEmpty(photo)) {
            mImageLoader.loadImage(photo, photoView, true);
        } else {
            photoView.setImageDrawable(null);
        }

        titleView.setText(carTitle == null ? "?" : carTitle);

        if (subtitleView != null) {
            subtitleView.setText(getString(R.string.car_date_posted) + " " + car.getDatePosted());
        }

        if (shortSubtitleView != null) {
            shortSubtitleView.setText(getString(R.string.car_date_posted) + " " + car.getDatePosted());
        }

        if (snippetView != null) {
            snippetView.setText(Html.fromHtml(carDescription));
        }

        view.findViewById(R.id.indicator_checked).setVisibility(checked ? View.VISIBLE
                : View.INVISIBLE);

        final View finalPhotoView = photoView;

        carTargetView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallbacks.onCarSelected(carId, finalPhotoView);
            }
        });

        if (dataIndex > mMaxDataIndexAnimated) {
            mMaxDataIndexAnimated = dataIndex;
        }
    }

    public void setContentTopClearance(int topClearance) {
        mCollectionView.setContentTopClearance(topClearance);
    }

    private class Preloader extends ListPreloader<String> {
        private int[] photoDimens;

        private int displayCols;

        public Preloader(int maxPreload) {
            super(maxPreload);
        }

        public void setDisplayCols(int displayCols) {
            this.displayCols = displayCols;
        }

        public boolean isDimensSet() {
            return photoDimens != null;
        }

        public void setDimens(int width, int height) {
            if (photoDimens == null) {
                photoDimens = new int[]{width, height};
            }
        }

        @Override
        protected int[] getDimensions(String s) {
            return photoDimens;
        }

        @Override
        protected List<String> getItems(int start, int end) {
            int keynoteDataOffset = (displayCols - 1);

            int dataStart = start * displayCols - keynoteDataOffset;

            int dataEnd = end * displayCols - keynoteDataOffset;

            List<String> urls = new ArrayList<String>();

            if (mFilteredCars.size() != 0) {
                for (int i = dataStart; i < dataEnd && i < mFilteredCars.size(); i++) {
                    if (i > 0) {
                        urls.add(mFilteredCars.get(i).getPhotoUrl());
                    }
                }
            }

            return urls;
        }

        @Override
        protected GenericRequestBuilder getRequestBuilder(String url) {
            return mImageLoader.beginImageLoad(url, null, true);
        }
    }
}
