package com.webshore.test.ui;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.webshore.test.R;
import com.webshore.test.ui.widget.NumberRatingBar;
import com.webshore.test.util.PrefUtils;

import static com.webshore.test.util.LogUtils.makeLogTag;

public class CarFeedbackFragment extends Fragment {
    private static final String TAG = makeLogTag(CarDetailActivity.class);

    private String mCarId;

    private String mTitleString;

    private TextView mTitle;

    private RatingBar mCarRatingFeedbackBar;

    private NumberRatingBar mQ1FeedbackBar;

    private EditText mComments;

    public CarFeedbackFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCarId = (String) getArguments().get(CarFeedbackActivity.EXTRA_CAR_ID);

        mTitleString = (String) getArguments().get(CarFeedbackActivity.EXTRA_CAR_TITLE);

        if (mCarId == null) {
            return;
        }

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_car_feedback, null);

        mTitle = (TextView) rootView.findViewById(R.id.car_title);

        mCarRatingFeedbackBar = (RatingBar) rootView.findViewById(R.id.rating_bar_0);

        mQ1FeedbackBar = (NumberRatingBar) rootView.findViewById(R.id.rating_bar_1);

        mComments = (EditText) rootView.findViewById(R.id.car_feedback_comments);

        mTitle.setText(mTitleString);

        rootView.findViewById(R.id.submit_feedback_button).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        submitAllFeedback();

                        getActivity().finish();
                    }
                }
        );

        return rootView;
    }

    void submitAllFeedback() {
        int rating = (int) mCarRatingFeedbackBar.getRating();

        int q1Answer = mQ1FeedbackBar.getProgress();

        String comments = mComments.getText().toString();

        if (rating + q1Answer != 0 || !comments.isEmpty()) {
            PrefUtils.setGaveFeedback(getActivity(), mCarId, true);
        }
    }
}
