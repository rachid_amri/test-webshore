package com.webshore.test.ui;

import android.app.FragmentManager;
import android.app.SearchManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.webshore.test.R;
import com.webshore.test.model.TagMetadata;
import com.webshore.test.provider.TestContract;

import static com.webshore.test.util.LogUtils.LOGD;
import static com.webshore.test.util.LogUtils.LOGW;
import static com.webshore.test.util.LogUtils.makeLogTag;

public class SearchActivity extends BaseActivity implements CarsFragment.Callbacks {
    private static final String TAG = makeLogTag("SearchActivity");

    CarsFragment mCarsFragment = null;

    SearchView mSearchView = null;

    String mQuery = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_search);

        Toolbar toolbar = getActionBarToolbar();

        toolbar.setTitle(R.string.title_search);

        toolbar.setNavigationIcon(R.drawable.ic_up);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateUpToFromChild(SearchActivity.this,
                        IntentCompat.makeMainActivity(new ComponentName(SearchActivity.this,
                                BrowseCarsActivity.class)));
            }
        });

        FragmentManager fm = getFragmentManager();

        mCarsFragment = (CarsFragment) fm.findFragmentById(R.id.fragment_container);

        String query = getIntent().getStringExtra(SearchManager.QUERY);

        query = query == null ? "" : query;

        mQuery = query;

        if (mCarsFragment == null) {
            mCarsFragment = new CarsFragment();

            Bundle args = intentToFragmentArguments(
                    new Intent(Intent.ACTION_VIEW, TestContract.Cars.buildSearchUri(query)));

            mCarsFragment.setArguments(args);

            fm.beginTransaction().add(R.id.fragment_container, mCarsFragment).commit();
        }

        if (mSearchView != null) {
            mSearchView.setQuery(query, false);
        }

        overridePendingTransition(0, 0);
    }

    @Override
    public void onCarSelected(String carId, View clickedView) {
        Intent intent = new Intent(this, CarDetailActivity.class);

        intent.putExtra("CAR_ID", carId);

        getLUtils().startActivityWithTransition(intent,
                clickedView,
                CarDetailActivity.TRANSITION_NAME_PHOTO);
    }

    @Override
    public void onTagMetadataLoaded(TagMetadata metadata) {
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        onNewIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {
        LOGD(TAG, "SearchActivity.onNewIntent: " + intent);

        setIntent(intent);

        String query = intent.getStringExtra(SearchManager.QUERY);

        Bundle args = intentToFragmentArguments(
                new Intent(Intent.ACTION_VIEW, TestContract.Cars.buildSearchUri(query)));

        LOGD(TAG, "onNewIntent() now reloading cars fragment with args: " + args);

        Bundle carsArgs = new Bundle();

        carsArgs.putString("QUERY", query);

        mCarsFragment.reloadFromArguments(carsArgs, true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.search, menu);

        final MenuItem searchItem = menu.findItem(R.id.menu_search);

        if (searchItem != null) {
            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);

            final SearchView view = (SearchView) searchItem.getActionView();

            mSearchView = view;

            if (view == null) {
                LOGW(TAG, "Could not set up search view, view is null.");
            } else {
                view.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));

                view.setIconified(false);

                view.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                    @Override
                    public boolean onQueryTextSubmit(String s) {
                        view.clearFocus();

                        return true;
                    }

                    @Override
                    public boolean onQueryTextChange(String s) {
                        if (null != mCarsFragment) {
                            Bundle carsArgs = new Bundle();

                            carsArgs.putString("QUERY", s);

                            mCarsFragment.reloadFromArguments(carsArgs, true);
                        }

                        return true;
                    }
                });

                view.setOnCloseListener(new SearchView.OnCloseListener() {
                    @Override
                    public boolean onClose() {
                        finish();

                        return false;
                    }
                });
            }

            if (!TextUtils.isEmpty(mQuery)) {
                view.setQuery(mQuery, false);
            }
        }
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (isFinishing()) {
            overridePendingTransition(0, 0);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.menu_search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
