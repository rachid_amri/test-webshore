package com.webshore.test.ui;

import android.content.Intent;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.j256.ormlite.dao.Dao;
import com.webshore.test.R;
import com.webshore.test.model.Car;
import com.webshore.test.ui.widget.CheckableFrameLayout;
import com.webshore.test.ui.widget.MessageCardView;
import com.webshore.test.ui.widget.ObservableScrollView;
import com.webshore.test.util.ImageLoader;
import com.webshore.test.util.LogUtils;
import com.webshore.test.util.PrefUtils;
import com.webshore.test.util.UIUtils;
import com.bumptech.glide.request.bitmap.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.UUID;

public class CarDetailActivity extends BaseActivity implements
        ObservableScrollView.Callbacks {
    private static final String TAG = LogUtils.makeLogTag(CarDetailActivity.class);

    private static final int[] SECTION_HEADER_RES_IDS = {
            R.id.car_facts_header,
            R.id.car_owner_header
    };

    private static final float PHOTO_ASPECT_RATIO = 1.7777777f;

    public static final String TRANSITION_NAME_PHOTO = "photo";

    private Handler mHandler = new Handler();

    private String mCarId;

    private boolean mStarred;

    private View mScrollViewChild;
    private TextView mCarTitle;

    private TextView mCarOwner;

    private ObservableScrollView mScrollView;
    private CheckableFrameLayout mAddButton;

    private TextView mDescription;
    private View mHeaderBox;
    private View mDetailsContainer;

    private ImageLoader mNoPlaceholderImageLoader;
    private List<Runnable> mDeferredUiOperations = new ArrayList<Runnable>();

    private int mPhotoHeightPixels;
    private int mHeaderHeightPixels;
    private int mAddButtonHeightPixels;

    private boolean mHasPhoto;
    private String mPhotoUrl;
    private View mPhotoViewContainer;
    private ImageView mPhotoView;
    private int mCarColor;

    private boolean mAlreadyGaveFeedback = false;

    private static HashSet<String> sDismissedFeedbackCard = new HashSet<String>();

    private float mMaxHeaderElevation;
    private float mFABElevation;

    private ArrayList<Car> mCars;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        boolean shouldBeFloatingWindow = shouldBeFloatingWindow();

        if (shouldBeFloatingWindow) {
            setupFloatingWindow();
        }

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_car_detail);

        final Toolbar toolbar = getActionBarToolbar();

        toolbar.setNavigationIcon(shouldBeFloatingWindow
                ? R.drawable.ic_ab_close : R.drawable.ic_up);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        Dao<Car, UUID> carsDao = null;

        try {
            carsDao = getDatabaseHelper().getCarsDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        List<Car> carList = null;

        try {
            carList = carsDao.queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        mCars = new ArrayList<>();

        for (Car car : carList) {
            if (Integer.parseInt(car.getId()) < 100) {
                car.setColor(getResources().getColor(R.color.peugeot_tag_color));
            } else if (Integer.parseInt(car.getId()) < 200) {
                car.setColor(getResources().getColor(R.color.renault_tag_color));
            } else if (Integer.parseInt(car.getId()) < 300) {
                car.setColor(getResources().getColor(R.color.seat_tag_color));
            } else if (Integer.parseInt(car.getId()) < 400) {
                car.setColor(getResources().getColor(R.color.mazda_tag_color));
            } else if (Integer.parseInt(car.getId()) < 500) {
                car.setColor(getResources().getColor(R.color.bmw_tag_color));
            } else if (Integer.parseInt(car.getId()) < 600) {
                car.setColor(getResources().getColor(R.color.chevrolet_tag_color));
            } else if (Integer.parseInt(car.getId()) < 700) {
                car.setColor(getResources().getColor(R.color.mercedes_tag_color));
            } else if (Integer.parseInt(car.getId()) < 800) {
                car.setColor(getResources().getColor(R.color.audi_tag_color));
            } else if (Integer.parseInt(car.getId()) < 900) {
                car.setColor(getResources().getColor(R.color.lexus_tag_color));
            }

            mCars.add(car);
        }

        mCarId = getIntent().getStringExtra("CAR_ID");

        if (mCarId == null) {
            return;
        }

        mFABElevation = getResources().getDimensionPixelSize(R.dimen.fab_elevation);

        mMaxHeaderElevation = getResources().getDimensionPixelSize(
                R.dimen.car_detail_max_header_elevation);

        mHandler = new Handler();

        if (mNoPlaceholderImageLoader == null) {
            mNoPlaceholderImageLoader = new ImageLoader(this);
        }

        mScrollView = (ObservableScrollView) findViewById(R.id.scroll_view);

        mScrollView.addCallbacks(this);

        ViewTreeObserver vto = mScrollView.getViewTreeObserver();

        if (vto.isAlive()) {
            vto.addOnGlobalLayoutListener(mGlobalLayoutListener);
        }

        mScrollViewChild = findViewById(R.id.scroll_view_child);

        mScrollViewChild.setVisibility(View.INVISIBLE);

        mDetailsContainer = findViewById(R.id.details_container);

        mHeaderBox = findViewById(R.id.header_car);

        mCarTitle = (TextView) findViewById(R.id.car_title);

        mCarOwner = (TextView) findViewById(R.id.car_owner_text);

        mCarOwner.setText(Html.fromHtml(getResources().getString(R.string.car_owner_info)));

        mPhotoViewContainer = findViewById(R.id.car_photo_container);

        mPhotoView = (ImageView) findViewById(R.id.car_photo);

        mDescription = (TextView) findViewById(R.id.car_description);

        mStarred = PrefUtils.isCarFavorite(this, mCarId);

        mAddButton = (CheckableFrameLayout) findViewById(R.id.add_button);

        mAddButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean starred = !mStarred;

                PrefUtils.markCarFavorite(CarDetailActivity.this, mCarId, starred);

                showStarred(starred, true);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                    mAddButton.announceForAccessibility(starred ?
                            getString(R.string.car_details_a11y_car_added) :
                            getString(R.string.car_details_a11y_car_removed));
                }
            }
        });

        for (final Car car : mCars) {
            if (car.getId().equals(mCarId)) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        toolbar.setTitleTextColor(getResources().getColor(R.color.body_text_2_inverse));

                        toolbar.setTitle(car.getDatePosted());
                    }
                });

                mCarTitle.setText(car.getTitle());

                mCarColor = car.getColor();

                mPhotoUrl = car.getPhotoUrl();

                mDescription.setText(Html.fromHtml(car.getDescription()));

                if (mCarColor == 0) {
                    mCarColor = getResources().getColor(R.color.default_car_color);
                } else {
                    mCarColor = UIUtils.setColorAlpha(mCarColor, 255);
                }
            }
        }

        mHeaderBox.setBackgroundColor(mCarColor);

        getLUtils().setStatusBarColor(UIUtils.scaleColor(mCarColor, 0.8f, false));

        for (int resId : SECTION_HEADER_RES_IDS) {
            ((TextView) findViewById(resId)).setTextColor(mCarColor);
        }

        mPhotoViewContainer.setBackgroundColor(UIUtils.scaleCarColorToDefaultBG(mCarColor));

        if (!TextUtils.isEmpty(mPhotoUrl)) {
            mHasPhoto = true;

            mNoPlaceholderImageLoader.loadImage(mPhotoUrl, mPhotoView, new RequestListener<String>() {
                @Override
                public void onException(Exception e, String url, Target target) {
                    mHasPhoto = false;

                    recomputePhotoAndScrollingMetrics();
                }

                @Override
                public void onImageReady(String url, Target target, boolean b, boolean b2) {
                    recomputePhotoAndScrollingMetrics();
                }
            });

            recomputePhotoAndScrollingMetrics();
        } else {
            mHasPhoto = false;

            recomputePhotoAndScrollingMetrics();
        }

        mAddButton.setVisibility(View.VISIBLE);

        showStarredDeferred(mStarred, false);

        if (PrefUtils.isCarFavorite(this, mCarId) && !PrefUtils.hasGivenFeedback(this, mCarId) && !sDismissedFeedbackCard.contains(mCarId)) {
            showGiveFeedbackCard();
        }

        mHandler.post(new Runnable() {
            @Override
            public void run() {
                onScrollChanged(0, 0);

                mScrollViewChild.setVisibility(View.VISIBLE);
            }
        });

        ViewCompat.setTransitionName(mPhotoView, TRANSITION_NAME_PHOTO);
    }

    @Override
    public Intent getParentActivityIntent() {
        return new Intent(this, BrowseCarsActivity.class);
    }

    private void setupFloatingWindow() {
        WindowManager.LayoutParams params = getWindow().getAttributes();

        params.width = getResources().getDimensionPixelSize(R.dimen.car_details_floating_width);

        params.height = getResources().getDimensionPixelSize(R.dimen.car_details_floating_height);

        params.alpha = 1;

        params.dimAmount = 0.4f;

        params.flags |= WindowManager.LayoutParams.FLAG_DIM_BEHIND;

        getWindow().setAttributes(params);
    }

    private boolean shouldBeFloatingWindow() {
        Resources.Theme theme = getTheme();

        TypedValue floatingWindowFlag = new TypedValue();

        if (theme == null || !theme.resolveAttribute(R.attr.isFloatingWindow, floatingWindowFlag, true)) {
            return false;
        }

        return (floatingWindowFlag.data != 0);
    }

    private void recomputePhotoAndScrollingMetrics() {
        mHeaderHeightPixels = mHeaderBox.getHeight();

        mPhotoHeightPixels = 0;

        if (mHasPhoto) {
            mPhotoHeightPixels = (int) (mPhotoView.getWidth() / PHOTO_ASPECT_RATIO);

            mPhotoHeightPixels = Math.min(mPhotoHeightPixels, mScrollView.getHeight() * 2 / 3);
        }

        ViewGroup.LayoutParams lp;

        lp = mPhotoViewContainer.getLayoutParams();

        if (lp.height != mPhotoHeightPixels) {
            lp.height = mPhotoHeightPixels;

            mPhotoViewContainer.setLayoutParams(lp);
        }

        ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams)
                mDetailsContainer.getLayoutParams();

        if (mlp.topMargin != mHeaderHeightPixels + mPhotoHeightPixels) {
            mlp.topMargin = mHeaderHeightPixels + mPhotoHeightPixels;

            mDetailsContainer.setLayoutParams(mlp);
        }

        onScrollChanged(0, 0);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mScrollView == null) {
            return;
        }

        ViewTreeObserver vto = mScrollView.getViewTreeObserver();

        if (vto.isAlive()) {
            vto.removeGlobalOnLayoutListener(mGlobalLayoutListener);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (PrefUtils.hasGivenFeedback(this, mCarId)) {
            final MessageCardView giveFeedbackCardView = (MessageCardView) findViewById(R.id.give_feedback_card);

            if (giveFeedbackCardView != null) {
                giveFeedbackCardView.setVisibility(View.GONE);
            }

            invalidateOptionsMenu();
        }
    }

    private ViewTreeObserver.OnGlobalLayoutListener mGlobalLayoutListener
            = new ViewTreeObserver.OnGlobalLayoutListener() {
        @Override
        public void onGlobalLayout() {
            mAddButtonHeightPixels = mAddButton.getHeight();

            recomputePhotoAndScrollingMetrics();
        }
    };

    @Override
    public void onScrollChanged(int deltaX, int deltaY) {
        int scrollY = mScrollView.getScrollY();

        float newTop = Math.max(mPhotoHeightPixels, scrollY);

        mHeaderBox.setTranslationY(newTop);

        mAddButton.setTranslationY(newTop + mHeaderHeightPixels
                - mAddButtonHeightPixels / 2);

        float gapFillProgress = 1;

        if (mPhotoHeightPixels != 0) {
            gapFillProgress = Math.min(Math.max(UIUtils.getProgress(scrollY,
                    0,
                    mPhotoHeightPixels), 0), 1);
        }

        ViewCompat.setElevation(mHeaderBox, gapFillProgress * mMaxHeaderElevation);

        ViewCompat.setElevation(mAddButton, gapFillProgress * mMaxHeaderElevation
                + mFABElevation);

        mPhotoViewContainer.setTranslationY(scrollY * 0.5f);
    }

    private void showGiveFeedbackCard() {
        final MessageCardView messageCardView = (MessageCardView) findViewById(R.id.give_feedback_card);

        messageCardView.show();

        messageCardView.setListener(new MessageCardView.OnMessageCardButtonClicked() {
            @Override
            public void onMessageCardButtonClicked(String tag) {
                if ("GIVE_FEEDBACK".equals(tag)) {
                    Intent intent = getFeedbackIntent();

                    startActivity(intent);
                } else {
                    sDismissedFeedbackCard.add(mCarId);

                    messageCardView.dismiss();
                }
            }
        });
    }

    private Intent getFeedbackIntent() {
        Intent intent = new Intent(this, CarFeedbackActivity.class);

        intent.putExtra(CarFeedbackActivity.EXTRA_CAR_ID, mCarId);

        intent.putExtra(CarFeedbackActivity.EXTRA_CAR_TITLE, mCarTitle.getText());

        return intent;
    }

    private void showStarredDeferred(final boolean starred, final boolean allowAnimate) {
        mDeferredUiOperations.add(new Runnable() {
            @Override
            public void run() {
                showStarred(starred, allowAnimate);
            }
        });

        tryExecuteDeferredUiOperations();
    }

    private void showStarred(boolean starred, boolean allowAnimate) {
        mStarred = starred;

        mAddButton.setChecked(mStarred, allowAnimate);

        ImageView iconView = (ImageView) mAddButton.findViewById(R.id.add_icon);

        getLUtils().setOrAnimatePlusCheckIcon(iconView, starred, allowAnimate);

        mAddButton.setContentDescription(getString(starred
                ? R.string.remove_desc
                : R.string.add_desc));
    }

    private void tryExecuteDeferredUiOperations() {
        for (Runnable r : mDeferredUiOperations) {
            r.run();
        }

        mDeferredUiOperations.clear();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        if (!PrefUtils.hasGivenFeedback(this, mCarId)) {
            getMenuInflater().inflate(R.menu.car_detail, menu);
        }

        tryExecuteDeferredUiOperations();

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_feedback:
                Intent intent = getFeedbackIntent();

                startActivity(intent);

                return true;
        }

        return false;
    }
}
