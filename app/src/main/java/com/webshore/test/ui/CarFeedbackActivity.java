package com.webshore.test.ui;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.webshore.test.R;

public class CarFeedbackActivity extends SimpleSinglePaneActivity {
    public static final String EXTRA_CAR_ID = "car_id";

    public static final String EXTRA_CAR_TITLE = "car_title";

    private String mCarId = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mCarId = getIntent().getStringExtra(EXTRA_CAR_ID);

        Toolbar toolbar = getActionBarToolbar();

        toolbar.setTitle(R.string.title_car_feedback);

        toolbar.setNavigationIcon(R.drawable.ic_up);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(CarFeedbackActivity.this, CarDetailActivity.class);

                intent.putExtra("CAR_ID", mCarId);

                navigateUpToFromChild(CarFeedbackActivity.this, intent);
            }
        });
    }

    @Override
    protected int getContentViewResId() {
        return R.layout.activity_feedback;
    }

    @Override
    protected Fragment onCreatePane() {
        return new CarFeedbackFragment();
    }

    @Override
    public Intent getParentActivityIntent() {
        if (mCarId != null) {
            Intent intent = new Intent(CarFeedbackActivity.this, CarDetailActivity.class);

            intent.putExtra("CAR_ID", mCarId);

            return intent;
        } else {
            return new Intent(this, BrowseCarsActivity.class);
        }
    }
}
