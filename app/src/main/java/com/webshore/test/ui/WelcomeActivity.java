package com.webshore.test.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.webshore.test.R;
import com.webshore.test.util.PrefUtils;

public class WelcomeActivity extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_welcome);

        findViewById(R.id.button_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PrefUtils.markWelcomeInitiated(WelcomeActivity.this);

                Intent intent = new Intent(WelcomeActivity.this, BrowseCarsActivity.class);

                startActivity(intent);

                finish();
            }
        });
    }
}
