package com.webshore.test.ui;

import android.animation.ArgbEvaluator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.OvalShape;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.webshore.test.Config;
import com.webshore.test.R;
import com.webshore.test.model.Car;
import com.webshore.test.model.TagMetadata;
import com.webshore.test.provider.TestContract;
import com.webshore.test.ui.widget.CollectionView;
import com.webshore.test.ui.widget.DrawShadowFrameLayout;
import com.webshore.test.util.NetUtils;
import com.webshore.test.util.PrefUtils;
import com.webshore.test.util.UIUtils;
import com.webshore.test.util.XMLParser;
import com.j256.ormlite.dao.Dao;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static com.webshore.test.util.LogUtils.LOGD;
import static com.webshore.test.util.LogUtils.LOGE;
import static com.webshore.test.util.LogUtils.LOGW;
import static com.webshore.test.util.LogUtils.makeLogTag;

public class BrowseCarsActivity extends BaseActivity implements CarsFragment.Callbacks {
    private static final String TAG = makeLogTag(BrowseCarsActivity.class);

    private static final String STATE_FILTER_0 = "STATE_FILTER_0";

    private TagMetadata mTagMetadata = null;

    private boolean mSpinnerConfigured = false;

    private String[] mFilterTags = {""};

    private String[] mFilterTagsToRestore = {null};

    private BrowseSpinnerAdapter mTopLevelSpinnerAdapter = new BrowseSpinnerAdapter(true);

    private CarsFragment mCarsFrag = null;

    private DrawShadowFrameLayout mDrawShadowFrameLayout;

    private View mButterBar;

    private Handler mHandler;

    private int mHeaderColor = 0;

    private static boolean sInternetWarningShown;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        loadCars();

        setContentView(R.layout.activity_browse_cars);

        mHandler = new Handler();

        Toolbar toolbar = getActionBarToolbar();

        overridePendingTransition(0, 0);

        if (savedInstanceState != null) {
            mFilterTagsToRestore[0] = mFilterTags[0] = savedInstanceState.getString(STATE_FILTER_0);
        }

        toolbar.setTitle(null);

        mButterBar = findViewById(R.id.butter_bar);

        mDrawShadowFrameLayout = (DrawShadowFrameLayout) findViewById(R.id.main_content);

        registerHideableHeaderView(mButterBar);
    }

    @Override
    public boolean canSwipeRefreshChildScrollUp() {
        if (mCarsFrag != null) {
            return mCarsFrag.canCollectionViewScrollUp();
        }

        return super.canSwipeRefreshChildScrollUp();
    }

    private void checkShowStaleDataButterBar() {
        boolean mustShowBar = !NetUtils.isNetworkAvailable(this)
                && (!PrefUtils.hasShownFirstInternetWarning(this) || !PrefUtils.hasShownSecondInternetWarning(this)) && !sInternetWarningShown;

        if (!mustShowBar) {
            mButterBar.setVisibility(View.GONE);
        } else {
            UIUtils.setUpButterBar(mButterBar, getString(R.string.data_stale_warning),
                    getString(R.string.description_got_it), new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mButterBar.setVisibility(View.GONE);

                            updateFragContentTopClearance();
                        }
                    }
            );

            sInternetWarningShown = true;

            if (!PrefUtils.hasShownFirstInternetWarning(this)) {
                PrefUtils.setFirstInternetWarningShown(this);
            } else {
                PrefUtils.setSecondInternetWarningShown(this);
            }
        }

        updateFragContentTopClearance();
    }

    @Override
    protected int getSelfNavDrawerItem() {
        return NAVDRAWER_ITEM_INVALID;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        CollectionView collectionView = (CollectionView) findViewById(R.id.cars_collection_view);

        if (collectionView != null) {
            enableActionBarAutoHide(collectionView);
        }

        mCarsFrag = (CarsFragment) getFragmentManager().findFragmentById(
                R.id.cars_fragment);

        if (mCarsFrag != null && savedInstanceState == null) {
            Bundle args = intentToFragmentArguments(getIntent());

            mCarsFrag.reloadFromArguments(args, true);
        }

        registerHideableHeaderView(findViewById(R.id.headerbar));
    }

    @Override
    public void onTagMetadataLoaded(TagMetadata metadata) {
        mTagMetadata = metadata;

        if (mSpinnerConfigured) {
            mSpinnerConfigured = false;

            mFilterTagsToRestore[0] = mFilterTags[0];
        }

        trySetUpActionBarSpinner();
    }

    @Override
    protected void onResume() {
        super.onResume();

        checkShowStaleDataButterBar();
    }

    private void updateFragContentTopClearance() {
        CarsFragment frag = (CarsFragment) getFragmentManager().findFragmentById(
                R.id.cars_fragment);

        if (frag == null) {
            return;
        }

        final boolean butterBarVisible = mButterBar != null
                && mButterBar.getVisibility() == View.VISIBLE;

        int actionBarClearance = UIUtils.calculateActionBarSize(this);

        int butterBarClearance = butterBarVisible
                ? getResources().getDimensionPixelSize(R.dimen.butter_bar_height) : 0;

        int gridPadding = getResources().getDimensionPixelSize(R.dimen.browse_grid_padding);

        setProgressBarTopWhenActionBarShown(actionBarClearance + butterBarClearance);

        mDrawShadowFrameLayout.setShadowTopOffset(actionBarClearance + butterBarClearance);

        frag.setContentTopClearance(actionBarClearance + butterBarClearance + gridPadding);
    }

    private void trySetUpActionBarSpinner() {
        Toolbar toolbar = getActionBarToolbar();

        if (mSpinnerConfigured || mTagMetadata == null || toolbar == null) {
            LOGD(TAG, "Not configuring Action Bar spinner.");

            return;
        }

        LOGD(TAG, "Configuring Action Bar spinner.");

        mSpinnerConfigured = true;

        mTopLevelSpinnerAdapter.clear();

        mTopLevelSpinnerAdapter.addItem("", getString(R.string.all_cars), false, 0);

        int itemToSelect = -1;

        int i;

        for (i = 0; i < Config.Tags.BROWSE_CATEGORIES.length; i++) {
            String category = Config.Tags.BROWSE_CATEGORIES[i];

            String categoryTitle = getString(Config.Tags.BROWSE_CATEGORY_TITLE[i]);

            LOGD(TAG, "Processing tag category: " + category + ", title=" + categoryTitle);

            List<TagMetadata.Tag> tags = mTagMetadata.getTagsInCategory(category);

            if (tags != null) {
                mTopLevelSpinnerAdapter.addHeader(categoryTitle);

                for (TagMetadata.Tag tag : mTagMetadata.getTagsInCategory(category)) {
                    LOGD(TAG, "Adding item to spinner: " + tag.getId() + " --> " + tag.getName());

                    int tagColor = Config.Tags.CATEGORY_MANUFACTURERS.equals(category) ? tag.getColor() : 0;

                    mTopLevelSpinnerAdapter.addItem(tag.getId(), tag.getName(), true, tagColor);

                    if (!TextUtils.isEmpty(mFilterTagsToRestore[0]) && tag.getId().equals(mFilterTagsToRestore[0])) {
                        mFilterTagsToRestore[0] = null;

                        itemToSelect = mTopLevelSpinnerAdapter.getCount() - 1;
                    }
                }
            } else {
                LOGW(TAG, "Ignoring Explore category with no tags: " + category);
            }
        }

        mTopLevelSpinnerAdapter.addHeader("");

        mTopLevelSpinnerAdapter.addItem(TagMetadata.FAVORITES_TAG, getString(R.string.favorites), false, 0);

        mFilterTagsToRestore[0] = null;

        View spinnerContainer = LayoutInflater.from(this).inflate(R.layout.actionbar_spinner,
                toolbar, false);

        ActionBar.LayoutParams lp = new ActionBar.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);

        toolbar.addView(spinnerContainer, lp);

        Spinner spinner = (Spinner) spinnerContainer.findViewById(R.id.actionbar_spinner);

        spinner.setAdapter(mTopLevelSpinnerAdapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> spinner, View view, int position, long itemId) {
                onTopLevelTagSelected(mTopLevelSpinnerAdapter.getTag(position));
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        if (itemToSelect >= 0) {
            LOGD(TAG, "Restoring item selection to primary spinner: " + itemToSelect);

            spinner.setSelection(itemToSelect);
        }

        updateHeaderColor();
    }

    private void updateHeaderColor() {
        int oldHeaderColor;

        mHeaderColor = 0;

        for (String tag : mFilterTags) {
            if (tag != null) {
                TagMetadata.Tag tagObj = mTagMetadata.getTag(tag);

                if (tagObj != null && Config.Tags.CATEGORY_MANUFACTURERS.equals(tagObj.getCategory())) {
                    mHeaderColor = tagObj.getColor();
                }
            }
        }

        final View headerBar = findViewById(R.id.headerbar);

        oldHeaderColor = ((ColorDrawable) headerBar.getBackground()).getColor();

        final Window window = getWindow();

        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);

        ValueAnimator colorAnimation;

        if (mHeaderColor == 0) {
            colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), oldHeaderColor, getResources().getColor(R.color.theme_primary));
        } else {
            colorAnimation = ValueAnimator.ofObject(new ArgbEvaluator(), oldHeaderColor, mHeaderColor);
        }

        colorAnimation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animator) {
                headerBar.setBackgroundColor((Integer) animator.getAnimatedValue());

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (mHeaderColor == 0) {
                        window.setStatusBarColor(getThemedStatusBarColor());
                    } else {
                        window.setStatusBarColor(UIUtils.scaleColor((Integer) animator.getAnimatedValue(), 0.8f, false));
                    }
                }

                setNormalStatusBarColor(UIUtils.scaleColor((Integer) animator.getAnimatedValue(), 0.8f, false));
            }
        });

        colorAnimation.start();
    }

    private void onTopLevelTagSelected(String tag) {
        CarsFragment frag = (CarsFragment) getFragmentManager().findFragmentById(
                R.id.cars_fragment);

        if (frag == null) {
            LOGE(TAG, "Cars fragment not found!");

            return;
        }

        if (tag.equals(mFilterTags[0])) {
            return;
        }

        mFilterTags[0] = tag;

        updateHeaderColor();

        reloadFromFilters();
    }

    private void reloadFromFilters() {
        CarsFragment frag = (CarsFragment) getFragmentManager().findFragmentById(
                R.id.cars_fragment);

        if (frag == null) {
            LOGE(TAG, "Cars fragment not found! Can't reload from filters.");

            return;
        }

        Bundle args = BaseActivity.intentToFragmentArguments(
                new Intent(Intent.ACTION_VIEW, TestContract.Cars.buildTagFilterUri(
                        mFilterTags))
                        .putExtra(CarsFragment.EXTRA_NO_TRACK_BRANDING, mHeaderColor != 0)
                        .putExtra(CarsFragment.EXTRA_TAG, mFilterTags[0]));

        frag.reloadFromArguments(args, true);

        frag.animateReload();
    }

    @Override
    protected void onActionBarAutoShowOrHide(boolean shown) {
        super.onActionBarAutoShowOrHide(shown);

        mDrawShadowFrameLayout.setShadowVisible(shown, shown);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);

        getMenuInflater().inflate(R.menu.browse_cars, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_search:
                startActivity(new Intent(this, SearchActivity.class));

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCarSelected(String carId, View clickedView) {
        Intent intent = new Intent(this, CarDetailActivity.class);

        intent.putExtra("CAR_ID", carId);

        getLUtils().startActivityWithTransition(intent,
                clickedView,
                CarDetailActivity.TRANSITION_NAME_PHOTO);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(STATE_FILTER_0, mFilterTags[0]);
    }

    private class BrowseSpinnerItem {
        boolean isHeader;

        String tag, title;

        int color;

        boolean indented;

        BrowseSpinnerItem(boolean isHeader, String tag, String title, boolean indented, int color) {
            this.isHeader = isHeader;

            this.tag = tag;

            this.title = title;

            this.indented = indented;

            this.color = color;
        }
    }

    private class BrowseSpinnerAdapter extends BaseAdapter {
        private int mDotSize;

        private boolean mTopLevel;

        private BrowseSpinnerAdapter(boolean topLevel) {
            this.mTopLevel = topLevel;
        }

        private ArrayList<BrowseSpinnerItem> mItems = new ArrayList<BrowseSpinnerItem>();

        public void clear() {
            mItems.clear();
        }

        public void addItem(String tag, String title, boolean indented, int color) {
            mItems.add(new BrowseSpinnerItem(false, tag, title, indented, color));
        }

        public void addHeader(String title) {
            mItems.add(new BrowseSpinnerItem(true, "", title, false, 0));
        }

        @Override
        public int getCount() {
            return mItems.size();
        }

        @Override
        public Object getItem(int position) {
            return mItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        private boolean isHeader(int position) {
            return position >= 0 && position < mItems.size()
                    && mItems.get(position).isHeader;
        }

        @Override
        public View getDropDownView(int position, View view, ViewGroup parent) {
            if (view == null || !view.getTag().toString().equals("DROPDOWN")) {
                view = getLayoutInflater().inflate(R.layout.browse_spinner_item_dropdown,
                        parent, false);

                view.setTag("DROPDOWN");
            }

            TextView headerTextView = (TextView) view.findViewById(R.id.header_text);

            View dividerView = view.findViewById(R.id.divider_view);

            TextView normalTextView = (TextView) view.findViewById(android.R.id.text1);

            if (isHeader(position)) {
                headerTextView.setText(getTitle(position));

                if (TextUtils.isEmpty(getTitle(position))) {
                    headerTextView.setVisibility(View.GONE);
                } else {
                    headerTextView.setVisibility(View.VISIBLE);
                }

                normalTextView.setVisibility(View.GONE);

                dividerView.setVisibility(View.VISIBLE);
            } else {
                headerTextView.setVisibility(View.GONE);

                normalTextView.setVisibility(View.VISIBLE);

                dividerView.setVisibility(View.GONE);

                setUpNormalDropdownView(position, normalTextView);
            }

            return view;
        }

        @Override
        public View getView(int position, View view, ViewGroup parent) {
            if (view == null || !view.getTag().toString().equals("NON_DROPDOWN")) {
                view = getLayoutInflater().inflate(mTopLevel
                                ? R.layout.browse_spinner_item_actionbar
                                : R.layout.browse_spinner_item,
                        parent, false);

                view.setTag("NON_DROPDOWN");
            }

            TextView textView = (TextView) view.findViewById(android.R.id.text1);

            textView.setText(getTitle(position));

            return view;
        }

        private String getTitle(int position) {
            return position >= 0 && position < mItems.size() ? mItems.get(position).title : "";
        }

        private int getColor(int position) {
            return position >= 0 && position < mItems.size() ? mItems.get(position).color : 0;
        }

        private String getTag(int position) {
            return position >= 0 && position < mItems.size() ? mItems.get(position).tag : "";
        }

        private void setUpNormalDropdownView(int position, TextView textView) {
            textView.setText(getTitle(position));

            ShapeDrawable colorDrawable = (ShapeDrawable) textView.getCompoundDrawables()[2];

            int color = getColor(position);

            if (color == 0) {
                if (colorDrawable != null) {
                    textView.setCompoundDrawables(null, null, null, null);
                }
            } else {
                if (mDotSize == 0) {
                    mDotSize = getResources().getDimensionPixelSize(
                            R.dimen.tag_color_dot_size);
                }

                if (colorDrawable == null) {
                    colorDrawable = new ShapeDrawable(new OvalShape());

                    colorDrawable.setIntrinsicWidth(mDotSize);

                    colorDrawable.setIntrinsicHeight(mDotSize);

                    colorDrawable.getPaint().setStyle(Paint.Style.FILL);

                    textView.setCompoundDrawablesWithIntrinsicBounds(null, null, colorDrawable, null);
                }

                colorDrawable.getPaint().setColor(color);
            }
        }

        @Override
        public boolean isEnabled(int position) {
            return !isHeader(position);
        }

        @Override
        public int getItemViewType(int position) {
            return 0;
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }
    }

    @Override
    protected void requestDataRefresh() {
        super.requestDataRefresh();

        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (mCarsFrag != null) {
                    mCarsFrag.updateCollectionView(true);
                }
            }
        }, mRefreshDelay);
    }

    private void loadCars() {
        Dao<Car, UUID> carsDao = null;

        try {
            carsDao = getDatabaseHelper().getCarsDao();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        try {
            if (carsDao.queryForAll().size() != 0) {
                return;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        XMLParser parser = new XMLParser(getBaseContext());

        String xml = parser
                .getXmlFromAssets("cars.xml");

        Document doc = parser.getDomElement(xml);

        Element carsElement = (Element) doc.getElementsByTagName("Cars").item(0);

        NodeList peugeotElements = carsElement.getElementsByTagName("Peugeot");

        NodeList renaultElements = carsElement.getElementsByTagName("Renault");

        NodeList seatElements = carsElement.getElementsByTagName("SEAT");

        NodeList mazdaElements = carsElement.getElementsByTagName("Mazda");

        NodeList bmwElements = carsElement.getElementsByTagName("BMW");

        NodeList chevroletElements = carsElement.getElementsByTagName("Chevrolet");

        NodeList mercedesElements = carsElement.getElementsByTagName("Mercedes");

        NodeList audiElements = carsElement.getElementsByTagName("Audi");

        NodeList lexusElements = carsElement.getElementsByTagName("Lexus");

        for (int i = 0; i < peugeotElements.getLength(); i++) {
            Element peugeotElement = (Element) peugeotElements.item(i);

            Car peugeotCar = new Car();

            peugeotCar.setId(parser.getValue(peugeotElement, "Id"));

            peugeotCar.setTitle(parser.getValue(peugeotElement, "Title"));

            peugeotCar.setDescription(parser.getValue(peugeotElement, "Description"));

            peugeotCar.setDatePosted(parser.getValue(peugeotElement, "DatePosted"));

            peugeotCar.setPhotoUrl(parser.getValue(peugeotElement, "PhotoUrl"));

            try {
                carsDao.create(peugeotCar);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < renaultElements.getLength(); i++) {
            Element renaultElement = (Element) renaultElements.item(i);

            Car renaultCar = new Car();

            renaultCar.setId(parser.getValue(renaultElement, "Id"));

            renaultCar.setTitle(parser.getValue(renaultElement, "Title"));

            renaultCar.setDescription(parser.getValue(renaultElement, "Description"));

            renaultCar.setDatePosted(parser.getValue(renaultElement, "DatePosted"));

            renaultCar.setPhotoUrl(parser.getValue(renaultElement, "PhotoUrl"));

            try {
                carsDao.create(renaultCar);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < seatElements.getLength(); i++) {
            Element seatElement = (Element) seatElements.item(i);

            Car seatCar = new Car();

            seatCar.setId(parser.getValue(seatElement, "Id"));

            seatCar.setTitle(parser.getValue(seatElement, "Title"));

            seatCar.setDescription(parser.getValue(seatElement, "Description"));

            seatCar.setDatePosted(parser.getValue(seatElement, "DatePosted"));

            seatCar.setPhotoUrl(parser.getValue(seatElement, "PhotoUrl"));

            try {
                carsDao.create(seatCar);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < mazdaElements.getLength(); i++) {
            Element mazdaElement = (Element) mazdaElements.item(i);

            Car mazdaCar = new Car();

            mazdaCar.setId(parser.getValue(mazdaElement, "Id"));

            mazdaCar.setTitle(parser.getValue(mazdaElement, "Title"));

            mazdaCar.setDescription(parser.getValue(mazdaElement, "Description"));

            mazdaCar.setDatePosted(parser.getValue(mazdaElement, "DatePosted"));

            mazdaCar.setPhotoUrl(parser.getValue(mazdaElement, "PhotoUrl"));

            try {
                carsDao.create(mazdaCar);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < bmwElements.getLength(); i++) {
            Element bmwElement = (Element) bmwElements.item(i);

            Car bmwCar = new Car();

            bmwCar.setId(parser.getValue(bmwElement, "Id"));

            bmwCar.setTitle(parser.getValue(bmwElement, "Title"));

            bmwCar.setDescription(parser.getValue(bmwElement, "Description"));

            bmwCar.setDatePosted(parser.getValue(bmwElement, "DatePosted"));

            bmwCar.setPhotoUrl(parser.getValue(bmwElement, "PhotoUrl"));

            try {
                carsDao.create(bmwCar);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < chevroletElements.getLength(); i++) {
            Element chevroletElement = (Element) chevroletElements.item(i);

            Car chevroletCar = new Car();

            chevroletCar.setId(parser.getValue(chevroletElement, "Id"));

            chevroletCar.setTitle(parser.getValue(chevroletElement, "Title"));

            chevroletCar.setDescription(parser.getValue(chevroletElement, "Description"));

            chevroletCar.setDatePosted(parser.getValue(chevroletElement, "DatePosted"));

            chevroletCar.setPhotoUrl(parser.getValue(chevroletElement, "PhotoUrl"));

            try {
                carsDao.create(chevroletCar);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < mercedesElements.getLength(); i++) {
            Element mercedesElement = (Element) mercedesElements.item(i);

            Car mercedesCar = new Car();

            mercedesCar.setId(parser.getValue(mercedesElement, "Id"));

            mercedesCar.setTitle(parser.getValue(mercedesElement, "Title"));

            mercedesCar.setDescription(parser.getValue(mercedesElement, "Description"));

            mercedesCar.setDatePosted(parser.getValue(mercedesElement, "DatePosted"));

            mercedesCar.setPhotoUrl(parser.getValue(mercedesElement, "PhotoUrl"));

            try {
                carsDao.create(mercedesCar);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < audiElements.getLength(); i++) {
            Element audiElement = (Element) audiElements.item(i);

            Car audiCar = new Car();

            audiCar.setId(parser.getValue(audiElement, "Id"));

            audiCar.setTitle(parser.getValue(audiElement, "Title"));

            audiCar.setDescription(parser.getValue(audiElement, "Description"));

            audiCar.setDatePosted(parser.getValue(audiElement, "DatePosted"));

            audiCar.setPhotoUrl(parser.getValue(audiElement, "PhotoUrl"));

            try {
                carsDao.create(audiCar);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < lexusElements.getLength(); i++) {
            Element lexusElement = (Element) lexusElements.item(i);

            Car lexusCar = new Car();

            lexusCar.setId(parser.getValue(lexusElement, "Id"));

            lexusCar.setTitle(parser.getValue(lexusElement, "Title"));

            lexusCar.setDescription(parser.getValue(lexusElement, "Description"));

            lexusCar.setDatePosted(parser.getValue(lexusElement, "DatePosted"));

            lexusCar.setPhotoUrl(parser.getValue(lexusElement, "PhotoUrl"));

            try {
                carsDao.create(lexusCar);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
