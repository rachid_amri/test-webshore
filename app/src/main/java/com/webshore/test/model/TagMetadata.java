package com.webshore.test.model;

import android.content.Context;

import com.webshore.test.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class TagMetadata {
    public static final String CATEGORY_MANUFACTURERS = "CATEGORY_MANUFACTURERS";

    public static final String FAVORITES_TAG = "FAVORITES";

    public static final String PEUGEOT_TAG_ID = "0";

    public static final String RENAULT_TAG_ID = "1";

    public static final String SEAT_TAG_ID = "2";

    public static final String MAZDA_TAG_ID = "3";

    public static final String BMW_TAG_ID = "4";

    public static final String CHEVROLET_TAG_ID = "5";

    public static final String MERCEDES_TAG_ID = "6";

    public static final String AUDI_TAG_ID = "7";

    public static final String LEXUS_TAG_ID = "8";

    private Context mContext;

    HashMap<String, ArrayList<Tag>> mTagsInCategory = new HashMap<>();

    HashMap<String, Tag> mTagsById = new HashMap<>();

    public TagMetadata(Context ctx) {
        mContext = ctx;

        Tag peugeotTag = new Tag(PEUGEOT_TAG_ID, mContext.getString(R.string.peugeot_tag_name),
                CATEGORY_MANUFACTURERS, 0, mContext.getString(R.string.peugeot_tag_name),
                mContext.getResources().getColor(R.color.peugeot_tag_color));

        Tag renaultTag = new Tag(RENAULT_TAG_ID, mContext.getString(R.string.renault_tag_name),
                CATEGORY_MANUFACTURERS, 1, mContext.getString(R.string.renault_tag_name),
                mContext.getResources().getColor(R.color.renault_tag_color));

        Tag seatTag = new Tag(SEAT_TAG_ID, mContext.getString(R.string.seat_tag_name),
                CATEGORY_MANUFACTURERS, 2, mContext.getString(R.string.seat_tag_name),
                mContext.getResources().getColor(R.color.seat_tag_color));

        Tag mazdaTag = new Tag(MAZDA_TAG_ID, mContext.getString(R.string.mazda_tag_name),
                CATEGORY_MANUFACTURERS, 3, mContext.getString(R.string.mazda_tag_name),
                mContext.getResources().getColor(R.color.mazda_tag_color));

        Tag bmwTag = new Tag(BMW_TAG_ID, mContext.getString(R.string.bmw_tag_name),
                CATEGORY_MANUFACTURERS, 4, mContext.getString(R.string.bmw_tag_name),
                mContext.getResources().getColor(R.color.bmw_tag_color));

        Tag chevroletTag = new Tag(CHEVROLET_TAG_ID, mContext.getString(R.string.chevrolet_tag_name),
                CATEGORY_MANUFACTURERS, 5, mContext.getString(R.string.chevrolet_tag_name),
                mContext.getResources().getColor(R.color.chevrolet_tag_color));

        Tag mercedesTag = new Tag(MERCEDES_TAG_ID, mContext.getString(R.string.mercedes_tag_name),
                CATEGORY_MANUFACTURERS, 6, mContext.getString(R.string.mercedes_tag_name),
                mContext.getResources().getColor(R.color.mercedes_tag_color));

        Tag audiTag = new Tag(AUDI_TAG_ID, mContext.getString(R.string.audi_tag_name),
                CATEGORY_MANUFACTURERS, 7, mContext.getString(R.string.audi_tag_name),
                mContext.getResources().getColor(R.color.audi_tag_color));

        Tag lexusTag = new Tag(LEXUS_TAG_ID, mContext.getString(R.string.lexus_tag_name),
                CATEGORY_MANUFACTURERS, 8, mContext.getString(R.string.lexus_tag_name),
                mContext.getResources().getColor(R.color.lexus_tag_color));

        mTagsById.put(PEUGEOT_TAG_ID, peugeotTag);

        mTagsById.put(RENAULT_TAG_ID, renaultTag);

        mTagsById.put(SEAT_TAG_ID, seatTag);

        mTagsById.put(MAZDA_TAG_ID, mazdaTag);

        mTagsById.put(BMW_TAG_ID, bmwTag);

        mTagsById.put(CHEVROLET_TAG_ID, chevroletTag);

        mTagsById.put(MERCEDES_TAG_ID, mercedesTag);

        mTagsById.put(AUDI_TAG_ID, audiTag);

        mTagsById.put(LEXUS_TAG_ID, lexusTag);

        mTagsInCategory.put(CATEGORY_MANUFACTURERS, new ArrayList<Tag>());

        mTagsInCategory.get(CATEGORY_MANUFACTURERS).add(peugeotTag);

        mTagsInCategory.get(CATEGORY_MANUFACTURERS).add(renaultTag);

        mTagsInCategory.get(CATEGORY_MANUFACTURERS).add(seatTag);

        mTagsInCategory.get(CATEGORY_MANUFACTURERS).add(mazdaTag);

        mTagsInCategory.get(CATEGORY_MANUFACTURERS).add(bmwTag);

        mTagsInCategory.get(CATEGORY_MANUFACTURERS).add(chevroletTag);

        mTagsInCategory.get(CATEGORY_MANUFACTURERS).add(mercedesTag);

        mTagsInCategory.get(CATEGORY_MANUFACTURERS).add(audiTag);

        mTagsInCategory.get(CATEGORY_MANUFACTURERS).add(lexusTag);
    }

    public Tag getTag(String tagId) {
        return mTagsById.containsKey(tagId) ? mTagsById.get(tagId) : null;
    }

    public List<Tag> getTagsInCategory(String category) {
        return mTagsInCategory.containsKey(category) ?
                Collections.unmodifiableList(mTagsInCategory.get(category)) : null;
    }

    static public class Tag implements Comparable<Tag> {
        private String mId;

        private String mName;

        private String mCategory;

        private int mOrderInCategory;

        private String mAbstract;

        private int mColor;

        public Tag(String id, String name, String category, int orderInCategory, String _abstract,
                   int color) {
            mId = id;

            mName = name;

            mCategory = category;

            mOrderInCategory = orderInCategory;

            mAbstract = _abstract;

            mColor = color;
        }

        public String getId() {
            return mId;
        }

        public String getName() {
            return mName;
        }

        public String getCategory() {
            return mCategory;
        }

        public int getOrderInCategory() {
            return mOrderInCategory;
        }

        public String getAbstract() {
            return mAbstract;
        }

        public int getColor() {
            return mColor;
        }

        @Override
        public int compareTo(Tag another) {
            return mOrderInCategory - another.mOrderInCategory;
        }
    }
}
