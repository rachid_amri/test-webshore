package com.webshore.test.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.webshore.test.util.LogUtils;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;
import java.util.UUID;

import static com.webshore.test.util.LogUtils.LOGE;

public class DBhelper extends OrmLiteSqliteOpenHelper {
    private static final String TAG = LogUtils.makeLogTag(DBhelper.class);

    private static final String DATABASE_NAME = "WebShore.db";

    private static final int DATABASE_VERSION = 1;

    private Dao<Car, UUID> mCarsDao;

    public DBhelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public Dao<Car, UUID> getCarsDao() throws SQLException {
        if (mCarsDao == null) {
            mCarsDao = this.getDao(Car.class);
        }

        return mCarsDao;
    }

    @Override
    public void onCreate(SQLiteDatabase database, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Car.class);
        } catch (Exception e) {
            LOGE(TAG, "Can't create database", e);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, Car.class, true);

            TableUtils.createTable(connectionSource, Car.class);
        } catch (SQLException e) {
            LOGE(TAG, "Can't upgrade database", e);
        }
    }
}
