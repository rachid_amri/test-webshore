package com.webshore.test.provider;

import android.net.Uri;
import android.provider.BaseColumns;
import android.text.TextUtils;

public class TestContract {
    public static final String CONTENT_AUTHORITY = "com.webshore.test";

    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);

    private static final String PATH_CARS = "cars";

    private static final String PATH_SEARCH = "search";

    public static class Cars {
        public static final String QUERY_PARAMETER_TAG_FILTER = "filter";

        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_CARS).build();

        public static Uri buildTagFilterUri(String[] requiredTags) {
            StringBuilder sb = new StringBuilder();
            for (String tag : requiredTags) {
                if (TextUtils.isEmpty(tag)) continue;
                if (sb.length() > 0) {
                    sb.append(",");
                }
                sb.append(tag.trim());
            }
            if (sb.length() == 0) {
                return CONTENT_URI;
            } else {
                return CONTENT_URI.buildUpon().appendQueryParameter(QUERY_PARAMETER_TAG_FILTER,
                        sb.toString()).build();
            }
        }

        public static Uri buildSearchUri(String query) {
            if (null == query) {
                query = "";
            }

            query = query.replaceAll(" +", " *") + "*";

            return CONTENT_URI.buildUpon()
                    .appendPath(PATH_SEARCH).appendPath(query).build();
        }
    }

    private TestContract() {
    }
}
